<?php 
use App\Controllers\Pages;


class PagesTest extends PHPUnit_Framework_TestCase
{
	
	function __construct()
	{
		$this->pages = new Pages();
	}
	public function testRenderReturnHelloWorld()
	{
		$expected = "hello world";
		$this->assertEquals($expected,$this->pages->render());
	}

	public function testReturnTrueReturnsTrue() 
	{
		$expected = true;
		$this->assertTrue($this->pages->returnTrue());
	}
	public function testReturnArrayReturnsValidArray()
	{
		$this->assertTrue(is_array($this->pages->returnArray()));
	}

	public function testReturnArrayReturnNotEmptyArray() 
	{
		$this->assertTrue(count($this->pages->returnArray()) > 0);
	}
}
?>